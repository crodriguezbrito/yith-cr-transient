<div class="yith-notices-main">
    <ul>
    <?php foreach ( $feed_notices as $item ) : ?>
        <li>
            <a href="<?php echo esc_url( $item['permalink'] ); ?>">
                <?php echo esc_html( $item['title'] ); ?>
            </a>
        </li>
    <?php endforeach; ?>
    </ul>
</div>