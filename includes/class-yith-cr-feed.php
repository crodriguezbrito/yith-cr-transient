<?php
/*
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_CR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_CR_Feed' ) ) {

	class YITH_CR_Feed {

        /**
		 * Main Instance
		 *
		 * @var YITH_CR_Feed
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        
        /**
         * Main plugin Instance
         *
         * @return YITH_CR_Feed Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_CR_Feed constructor.
		 */
		private function __construct() {
            

        }
        
        public function get_feed_rss() {

            $notices = get_transient( 'yith_cr_notices_transient' );
            if ( !$notices ) {

                $rss_notice  = "http://feeds.bbci.co.uk/mundo/rss.xml";
                $rss            = fetch_feed(  $rss_notice );

                
                if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly
                 
                    // Figure out how many total items there are, but limit it to 5. 
                    $maxitems = $rss->get_item_quantity(); 
                 
                    // Build an array of all the items, starting with element 0 (first element).
                    $rss_items = $rss->get_items( 0, $maxitems );
                 
                endif;                
                foreach ( $rss_items as $item ) {
                    $object = array(
                        'title' => esc_html( $item->get_title() ),
                        'description' => esc_html( $item->get_description() ),
                        'permalink'   => esc_url( $item->get_permalink() ),
                    );
                    $notices[] = $object;
                
                }
                set_transient( 'yith_cr_notices_transient', $notices, 3 * HOUR_IN_SECONDS );
            }

            return $notices;

        }

	
	}	
}