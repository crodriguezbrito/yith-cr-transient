<?php
/*
 * This file belongs to the YITH PS Plugin Skeleton.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_CR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_CR_Datatables_Admin' ) ) {

	class YITH_CR_Datatables_Admin {

        /**
		 * Main Instance
		 *
		 * @var YITH_CR_Datatables_Admin
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        
        /**
         * Main plugin Instance
         *
         * @return YITH_CR_Datatables_Admin Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_CR_Datatables_Admin constructor.
		 */
		private function __construct() {


		}


		/**
		 * Setup the meta boxes
		 */
		public function add_meta_boxes() {
			add_meta_box( 'yith-ps-additional-information', __( 'Additional information', 'yith-plugin-skeleton' ), array(
				$this,
				'view_meta_boxes'
			), YITH_PS_Post_Types::$post_type );
		}


	
	}	
}