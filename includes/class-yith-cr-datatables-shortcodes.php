<?php
/*
 * This file belongs to the YITH CR Datatables.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_CR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

/**
 * @class      YITH CR Datatables Shortcodes
 * @package    Yithemes
 * @since      Version 1.0.0
 * @author     Carlos Rodriguez <carlos.rodriguez@yithemes.com>
 *
 */
if ( ! class_exists( 'YITH_CR_Datatables_Shortcodes' ) ) {
/**
 * Class YITH_CR_Datatables_Shortcodes
 *
 * @author Carlos Rodriguez <carlos.rodriguez@yithemes.com>
 */
	class YITH_CR_Datatables_Shortcodes {

		public static function init() {
			$shortcodes = array(
				'yith_cr_last_notices'			=> __CLASS__ . '::print_last_notices',
			);
			foreach ( $shortcodes as $shortcode => $function ) {
				add_shortcode( $shortcode, $function );
			}
        }
        
		
		public static function print_last_notices() {

			$fee_instance = YITH_CR_Feed::get_instance();

			$feed_notices = $fee_instance->get_feed_rss();
			
			ob_start();

			yith_cr_get_template( '/frontend/notices.php', array( 'feed_notices' => $feed_notices));

			return ob_get_clean();

		}

	}
}