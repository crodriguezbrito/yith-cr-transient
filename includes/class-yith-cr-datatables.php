<?php
/*
 * This file belongs to the YITH CR Datatables.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */
if ( ! defined( 'YITH_CR_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_CR_Datatables' ) ) {
	class YITH_CR_Datatables {

        /**
		 * Main Instance
		 *
		 * @var YITH_CR_Datatables
		 * @since 1.0
		 * @access private
		 */

		private static $instance;
        /**
		 * Main Admin Instance
		 *
		 * @var YITH_CR_Datatables_Admin
		 * @since 1.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_CR_Datatables_Frontend
		 * @since 1.0
		 */
		public $frontend = null;
		
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_CR_DB_Handler
		 * @since 1.0
		 */
        public $handler = null;
        
        /**
         * Main plugin Instance
         *
         * @return YITH_CR_Datatables Main instance
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

			// ternary operator --> https://www.codementor.io/@sayantinideb/ternary-operator-in-php-how-to-use-the-php-ternary-operator-x0ubd3po6 
        }
        
		/**
		 * YITH_PS_Plugin_Skeleton constructor.
		 */
		private function __construct() {


            $require = apply_filters('yith_ps_require_class',
				 array(
					'common'   => array(
						'includes/class-yith-cr-datatables-shortcodes.php',
						'includes/functions.php',
						'includes/class-yith-cr-feed.php',
					),
					'admin' => array(
						'includes/class-yith-cr-datatables-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-cr-datatables-frontend.php',
					),
				)
			);

			$this->_require($require);

			$this->init_classes();
				
			/* 
				Here set any other hooks ( actions or filters you'll use on this class)
			*/
			add_action('admin_post_nopriv_yith_cr_send_form', array($this, 'register_user' ));
			add_action('admin_post_yith_cr_send_form', array($this, 'register_user' ));
			
			// Finally call the init function
			$this->init();

		
		}
	
		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param $main_classes array The require classes file path
		 *
		 * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
		 * @since  1.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require($main_classes)
		{
			foreach ($main_classes as $section => $classes) {
				foreach ($classes as $class) {
					if ('common' == $section || ('frontend' == $section && !is_admin() || (defined('DOING_AJAX') && DOING_AJAX)) || ('admin' == $section && is_admin()) && file_exists(YITH_CR_DIR_PATH . $class)) {
						require_once(YITH_CR_DIR_PATH . $class);
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
		 * @since  1.0
		 **/
		public function init_classes(){
			YITH_CR_Datatables_Shortcodes::init();			
		}

		/**
         * Function init()
         *
         * Instance the admin or frontend classes
         *
         * @author Carlos Rodríguez <carlos.rodriguez@yithemes.com>
         * @since  1.0
         * @return void
         * @access protected
         */
        public function init()
        {
            if (is_admin()) {
                $this->admin =  YITH_CR_Datatables_Admin::get_instance();
            }

            if (!is_admin() || (defined('DOING_AJAX') && DOING_AJAX)) {
                $this->frontend = YITH_CR_Datatables_Frontend::get_instance();
            }
		}
		
	}	
}
/**
 * Get the YITH_PS_Plugin_Skeleton instance
 *
 * @return YITH_CR_Datatables
 */
if ( ! function_exists( 'yith_cr_datatables' ) ) {
	function yith_cr_datatables() {
		return YITH_CR_Datatables::get_instance();
	}
}